package de.debuglevel.monitoring.monitors

import de.debuglevel.monitoring.ServiceState
import mu.KotlinLogging
import java.io.IOException
import java.net.*


class TcpMonitor : Monitor {
    private val logger = KotlinLogging.logger {}

    /**
     * Timeout in milliseconds
     */
    private val timeout = 500

    override fun isValid(url: String): Boolean {
        val uri = try {
            URI(url)
        } catch (e: Exception) {
            return false
        }

        if (uri.port < 0 || uri.port > 65535) {
            return false
        }

        return true
    }

    override fun check(uri: URI): ServiceState {
        logger.debug { "Checking $uri..." }

        val state = Socket().use { socket ->
            socket.soTimeout = timeout

            try {
                logger.debug { "Connecting to socket with timeout ${timeout}ms..." }
                socket.connect(InetSocketAddress(uri.host, uri.port), timeout)
                logger.debug { "Connected to socket" }
                ServiceState.Up
            } catch (e: IOException) {
                logger.debug { "Connection to socket failed: ${e.message}" }
                ServiceState.Down
            } catch (e: SocketTimeoutException) {
                logger.debug { "Connection to socket timed out: ${e.message}" }
                ServiceState.Down
            } catch (e: UnknownHostException) {
                logger.debug { "Host is unknown: ${e.message}" }
                ServiceState.Down
            } catch (e: ConnectException) {
                logger.debug { "Could not connect socket: ${e.message}" }
                ServiceState.Down
            } catch (e: NoRouteToHostException) {
                logger.debug { "No route to host: ${e.message}" }
                ServiceState.Down
            } catch (e: Exception) {
                logger.warn(e) { "Unhandled exception" }
                ServiceState.Down
            }
        }

        logger.debug { "Checked $uri: $state" }
        return state
    }
}