package de.debuglevel.monitoring.monitoring

import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import jakarta.inject.Inject
import liquibase.pro.packaged.it
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.util.*

@MicronautTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MonitoringControllerTests {
    @Inject
    lateinit var monitoringClient: MonitoringClient

    fun monitoringProvider() = MonitoringTestDataProvider.validMonitoringItemProvider()

    @ParameterizedTest
    @MethodSource("monitoringProvider")
    fun `add monitoring`(monitoring: Monitoring) {
        // Arrange
        val addMonitoringRequest = AddMonitoringRequest(monitoring.url, monitoring.name)

        // Act
        val addedMonitoring = monitoringClient.add(addMonitoringRequest).block()

        // Assert
        Assertions.assertThat(addedMonitoring.name).isEqualTo(monitoring.name)
        Assertions.assertThat(addedMonitoring.name).isEqualTo(addMonitoringRequest.name)
        Assertions.assertThat(addedMonitoring.url).isEqualTo(monitoring.url)
        Assertions.assertThat(addedMonitoring.url).isEqualTo(addMonitoringRequest.url)
    }

    @ParameterizedTest
    @MethodSource("monitoringProvider")
    fun `get monitoring`(monitoring: Monitoring) {
        // Arrange
        val addMonitoringRequest = AddMonitoringRequest(monitoring.url, monitoring.name)
        val addedMonitoring = monitoringClient.add(addMonitoringRequest).block()

        // Act
        val getMonitoring = monitoringClient.get(addedMonitoring.id).block()

        // Assert
        Assertions.assertThat(getMonitoring.id).isEqualTo(addedMonitoring.id)
        Assertions.assertThat(getMonitoring.name).isEqualTo(monitoring.name)
        Assertions.assertThat(getMonitoring.name).isEqualTo(addedMonitoring.name)
    }

    @Test
    fun `get non-existing monitoring`() {
        // Arrange

        // Act
        val getMonitoringResponse = monitoringClient.get(Random.nextInt(1_000, 10_000)).block()

        // Assert
        Assertions.assertThat(getMonitoringResponse).isNull()
    }

    @Test
    fun `update monitoring`() {
        // Arrange
        val addMonitoringRequest = AddMonitoringRequest("Original Url", "Original Name")
        val addedMonitoring = monitoringClient.add(addMonitoringRequest).block()
        val updateMonitoringRequest = UpdateMonitoringRequest("Updated Name", "Updated Name")

        // Act
        val updatedMonitoring = monitoringClient.update(addedMonitoring.id!!, updateMonitoringRequest).block()
        val getMonitoring = monitoringClient.get(addedMonitoring.id!!).block()

        // Assert
        Assertions.assertThat(updatedMonitoring.id).isEqualTo(addedMonitoring.id)
        Assertions.assertThat(getMonitoring.id).isEqualTo(addedMonitoring.id)
        Assertions.assertThat(updatedMonitoring.name).isEqualTo(updateMonitoringRequest.name)
    }

    @Test
    fun `update non-existing monitoring`() {
        // Arrange
        val updateMonitoringRequest = UpdateMonitoringRequest("Updated Url", "Updated Name")

        // Act
        val getMonitoringResponse = monitoringClient.update(Random.nextInt(1_000, 10_000), updateMonitoringRequest).block()

        // Assert
        Assertions.assertThat(getMonitoringResponse).isNull()
    }

    @Test
    fun `get all monitorings`() {
        // Arrange
        monitoringProvider().forEach {
            monitoringClient.add(AddMonitoringRequest(it.url, it.name)).block()
        }

        // Act
        val getMonitorings = monitoringClient.getAll()

        // Assert
        Assertions.assertThat(getMonitorings).extracting<String> { x -> x.name }
            .containsAll(monitoringProvider().map { it.name }.toList())
    }
}