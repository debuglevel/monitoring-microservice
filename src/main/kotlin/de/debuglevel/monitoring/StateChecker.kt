package de.debuglevel.monitoring

import de.debuglevel.monitoring.monitoring.Monitoring
import de.debuglevel.monitoring.monitoring.MonitoringService
import de.debuglevel.monitoring.monitors.Monitor
import io.micronaut.scheduling.annotation.Scheduled
import jakarta.inject.Singleton
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import java.net.InetAddress
import java.time.LocalDateTime
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

@Singleton
class StateChecker(
    private val monitoringService: MonitoringService,
) {
    private val logger = KotlinLogging.logger {}

    /**
     * Checks all monitorings.
     */
    @ExperimentalTime
    @Scheduled(
        fixedDelay = "\${app.monitoring.checks.delay:60s}",
        initialDelay = "\${app.monitoring.checks.initial-delay:10s}"
    )
    fun checkAll() {
        logger.debug { "Checking all monitorings..." }

        val monitorings = monitoringService.list()
        val duration = measureTime {
            runBlocking {
                monitorings
                    .map {
                        // Use coroutines to run checks non-blocking/concurrently.
                        GlobalScope.launch {
                            check(it)
                            monitoringService.update(it.id!!, it)
                        }//.join() // DEBUG: Use a join() here to prevent concurrent execution
                    }
                    .map { it.join() }
            }
        }

        logger.debug { "Checked ${monitorings.count()} monitorings in $duration" }
    }

    /**
     * Checks [monitoring] and sets its state and information.
     */
    private fun check(monitoring: Monitoring) {
        logger.trace { "Checking $monitoring..." }

        val monitor = Monitor.get(monitoring.url)

        monitoring.serviceState = monitor.check(monitoring.uri)
        if (monitoring.serviceState == ServiceState.Up) {
            monitoring.lastSeen = monitoring.lastCheck
        }
        monitoring.ip = resolveHostname(monitoring.hostname)
        monitoring.lastCheck = LocalDateTime.now()

        logger.trace { "Checked $monitoring: ${monitoring.serviceState}" }
    }

    /**
     * Resolves the [hostname] to an IP.
     */
    private fun resolveHostname(hostname: String): String {
        logger.trace { "Resolving hostname '$hostname'..." }

        val ip = try {
            InetAddress.getByName(hostname).hostAddress
        } catch (e: Exception) {
            logger.warn(e) { "Could not resolve hostname $hostname to IP" }
            return "could not resolve hostname"
        }

        logger.trace { "Resolved hostname '$hostname': '$ip'" }
        return ip
    }
}