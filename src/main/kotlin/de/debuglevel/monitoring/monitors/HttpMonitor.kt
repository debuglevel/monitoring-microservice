package de.debuglevel.monitoring.monitors

import de.debuglevel.monitoring.ServiceState
import mu.KotlinLogging
import java.net.*

class HttpMonitor : Monitor {
    private val logger = KotlinLogging.logger {}

    /**
     * Timeout in milliseconds
     */
    private val timeout = 500

    override fun isValid(url: String): Boolean {
        logger.debug { "Checking validity of URL '$url'..." }

        // Using Apache Commons Validator would be nice, but it cannot disable failing on custom TLDs.
        val url = try {
            val url = URL(url)
            logger.debug { "Converted to URI with protocol '${url.protocol}', host '${url.host}', port '${url.port}', path '${url.path}', query '${url.query}'..." }
            url
        } catch (e: Exception) {
            logger.debug(e) { "Checking validity of URL '$url' failed with exception." }
            return false
        }

        if (url.host.isNullOrBlank()) {
            logger.debug { "Checking validity of URL '$url' failed with host '${url.host}' empty." }
            return false
        }

        // If port is not set, it is -1. It's the responsibility of the HTTP library to use the default port.
        if ((url.port != -1) && (url.port < 0 || url.port > 65535)) {
            logger.debug { "Checking validity of URL '$url' failed with port '${url.port}' too small or too large." }
            return false
        }

        if (url.protocol != "http" && url.protocol != "https") {
            logger.debug { "Checking validity of URL '$url' failed with scheme '${url.protocol}' != http or != https." }
            return false
        }

        logger.debug { "Checking validity of URL '$url' succeeded." }
        return true
    }

    override fun check(uri: URI): ServiceState {
        logger.debug { "Checking $uri..." }

        val url = uri.toURL()

        val state = try {
            logger.debug { "Opening connection on '$url'..." }
            val connection = url.openConnection() as HttpURLConnection
            connection.connectTimeout = timeout

            SslTrustModifier.relaxHostChecking(connection)

            logger.debug { "Getting response code..." }
            val statusCode = connection.responseCode

            connection.disconnect()

            if (statusCode < 400) {
                ServiceState.Up
            } else {
                ServiceState.Down
            }
        } catch (e: UnknownHostException) {
            logger.debug { "Host is down due to: ${e.message}" }
            ServiceState.Down
        } catch (e: ConnectException) {
            logger.debug { "Host is down due to: ${e.message}" }
            ServiceState.Down
        } catch (e: Exception) {
            logger.warn { "Unhandled exception: $e" }
            ServiceState.Down
        }

        logger.debug { "Checked $uri: $state" }
        return state
    }
}
