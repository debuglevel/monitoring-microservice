package de.debuglevel.monitoring.monitors

import de.debuglevel.monitoring.ServiceState
import mu.KotlinLogging
import java.net.URI

interface Monitor {
    /**
     * Checks a [uri] concerning its [ServiceState].
     */
    fun check(uri: URI): ServiceState

    /**
     * Checks if an [url] has a valid format.
     */
    fun isValid(url: String): Boolean

    companion object {
        private val logger = KotlinLogging.logger {}

        /**
         * Gets the appropriate monitor for a [url].
         */
        fun get(url: String): Monitor {
            logger.debug { "Getting monitor for $url..." }
            val uri = try {
                URI(url)
            } catch (e: Exception) {
                // e.g. java.net.URISyntaxException: Expected scheme name at index 0: ://
                throw InvalidMonitoringFormatException(url, e)
            }

            val monitor = when (uri.scheme) {
                "http" -> HttpMonitor()
                "https" -> HttpMonitor()
                "tcp" -> TcpMonitor()
                "icmp" -> IcmpMonitor()
                else -> throw UnsupportedMonitoringProtocolException(uri.scheme)
            }

            logger.debug { "Got monitor for $url: $monitor" }
            return monitor
        }
    }

    class UnsupportedMonitoringProtocolException(val scheme: String) : Exception("Protocol '$scheme' is not supported.")
    class InvalidMonitoringFormatException(url: String, inner: Exception) :
        Exception("Monitoring with URL '$url' has an invalid format: ${inner.message}")
}