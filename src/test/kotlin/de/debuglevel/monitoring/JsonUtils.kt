package de.debuglevel.monitoring

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.IOException

object JsonUtils {
    fun isValidJson(json: String): Boolean {
        var valid = false
        try {
            val parser = ObjectMapper()
                .factory
                .createParser(json)
            while (parser.nextToken() != null) {
            }
            valid = true
        } catch (ex: JsonParseException) {
//            ex.printStackTrace()
        } catch (ex: IOException) {
//            ex.printStackTrace()
        }

        return valid
    }
}