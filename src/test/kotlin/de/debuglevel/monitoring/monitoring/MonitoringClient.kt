package de.debuglevel.monitoring.monitoring

import io.micronaut.http.annotation.*
import io.micronaut.http.client.annotation.Client
import reactor.core.publisher.Mono
import java.util.*
import javax.validation.constraints.NotBlank

@Client("/monitorings")
interface MonitoringClient {
    @Get("/{id}")
    fun get(@NotBlank id: Int): Mono<GetMonitoringResponse>

    // TODO: Should probably be a reactive Flux<> instead
    @Get("/")
    fun getAll(): List<GetMonitoringResponse>

    @Post("/")
    fun add(@Body monitoring: AddMonitoringRequest): Mono<Monitoring>

    @Put("/{id}")
    fun update(@NotBlank id: Int, @Body monitoring: UpdateMonitoringRequest): Mono<Monitoring>

    @Delete("/{id}")
    fun delete(@NotBlank id: Int): Mono<Int>
}