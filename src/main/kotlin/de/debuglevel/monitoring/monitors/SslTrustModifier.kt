package de.debuglevel.monitoring.monitors

import mu.KotlinLogging
import java.net.HttpURLConnection
import java.security.cert.X509Certificate
import javax.net.ssl.*

object SslTrustModifier {
    private val logger = KotlinLogging.logger {}

    fun relaxHostChecking(urlConnection: HttpURLConnection) {
        logger.debug { "Relaxing host checking..." }

        if (urlConnection is HttpsURLConnection) {
            val factory = buildSslSocketFactory()
            urlConnection.sslSocketFactory = factory
            urlConnection.hostnameVerifier = TrustingHostnameVerifier()

            logger.debug { "Relaxed host checking" }
        } else {
            logger.debug { "Skipped relaxing host checking, as it is not a HttpsURLConnection" }
        }
    }

    private fun buildSslSocketFactory(): SSLSocketFactory {
        logger.debug { "Building SSLSocketFactory..." }

        val sslContext = SSLContext.getInstance("TLS").also {
            it.init(null, arrayOf<TrustManager>(AlwaysTrustManager()), null)
        }

        val factory = sslContext.socketFactory

        logger.debug { "Built SSLSocketFactory: $factory" }
        return factory
    }

    private class TrustingHostnameVerifier : HostnameVerifier {
        override fun verify(hostname: String, session: SSLSession): Boolean {
            return true
        }
    }

    private class AlwaysTrustManager : X509TrustManager {
        override fun checkClientTrusted(arg0: Array<X509Certificate>, arg1: String) {
        }

        override fun checkServerTrusted(arg0: Array<X509Certificate>, arg1: String) {
        }

        override fun getAcceptedIssuers(): Array<X509Certificate>? {
            return null
        }
    }
}